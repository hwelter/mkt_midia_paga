CREATE TABLE marketing_stg.dim_goal
  AS (
		with goal as(
			select 	row_number() over (order by substring(date,0,7)) as sk_goal,
					substring(date,0,7) as date, media, country, strategy, account, 
					sum(case  goal_name when 'Spend' then cast (cpc_budget.value as real) else null end) as Spend, 
					sum(case  goal_name when 'Conversions' then cast (cpc_budget.value as real) else null end) as Conversions,	
					sum(case  goal_name when 'Lead' then cast (cpc_budget.value as real) else null end) as Lead,				
					sum(case  goal_name when 'MQL' then cast (cpc_budget.value as real) else null end) as MQL,
					sum(case  goal_name when 'SAL' then cast (cpc_budget.value as real) else null end) as SAL 
			from marketing_raw.cpc_budget
			group by substring(date,0,7), media, country, strategy, account
			order by substring(date,0,7), media, country, strategy, account
		) 
		select * from goal 
  );