
DROP TABLE IF EXISTS dados_refinados.fat_media;

CREATE TABLE dados_refinados.fat_media
 STORED AS orc AS
    SELECT * 
    FROM (
        (SELECT 
        goal_daily.sk_tempo,
        supermetrics.sk_supermetrics,
        goal_daily.sk_goal_daily,
        goal_daily.midia,
        goal_daily.account,
        goal_daily.country,
        supermetrics.media_cost,
        goal_daily.spend_daily AS goal_daily_spend,
        goal_daily.conversions_daily AS goal_daily_conversions,
        goal_daily.lead_daily AS goal_daily_lead,
        goal_daily.mql_daily AS goal_daily_mql,
        goal_daily.sal_daily AS  goal_daily_sal
		
        FROM (
            SELECT MAX(id) AS sk_supermetrics,
            `date`, 
            country, UPPER(type) AS account, 
            UPPER(media) AS midia, 
            SUM(cost) AS media_cost 
            FROM dados_brutos.supermetrics 
            GROUP BY `date`, country, `type`, media
        ) AS supermetrics

        LEFT JOIN 

            (SELECT     
                    ROW_NUMBER () OVER (ORDER BY sk_tempo) AS sk_goal_daily,
                    sk_tempo,
                    dim_goal.month_year,
                    UPPER(media) AS midia,
                    country,
                    strategy,
                    UPPER(account) AS account,
                    (spend/(EXTRACT(DAY FROM last_day_of_month))) AS spend_daily,
                    (conversions/(EXTRACT(DAY FROM last_day_of_month))) AS conversions_daily,   
                    (lead/(EXTRACT(DAY FROM last_day_of_month))) AS lead_daily,     
                    (mql/(EXTRACT(DAY FROM last_day_of_month))) AS mql_daily,   
                    (sal/(EXTRACT(DAY FROM last_day_of_month))) AS sal_daily,
                    spend,
                    conversions,
                    lead,
                    mql,
                    sal

            FROM dados_refinados.dim_goal 

            LEFT JOIN (
                        SELECT  
                            SUBSTRING(sk_tempo,0,7) AS ano_mes, *
                        FROM dados_brutos.dim_tempo
                        ) AS tempo

            ON  (dim_goal.month_year = tempo.ano_mes)) AS goal_daily    
            
        ON (    goal_daily.sk_tempo LIKE supermetrics.`date` AND 
                goal_daily.midia = supermetrics.midia AND 
                goal_daily.account = supermetrics.account AND 
                goal_daily.country = supermetrics.country) WHERE sk_tempo IS NOT NULL) 
    ) AS fat_media
        
ANALYZE TABLE dados_refinados.fat_media COMPUTE STATISTICS;

