SELECT ROW_NUMBER() OVER (PARTITION by 0) as sk_media,upper(source) as media, upper(country) as country, upper(lead_channel) as account

FROM marketing_stg.dim_conversions group by source, country, lead_channel

