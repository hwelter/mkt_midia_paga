with dim_conversions as (
	select * from marketing_stg.dim_conversions
),

dim_qualified_leads as (
	select * from marketing_stg.dim_qualified_leads
),

fat_leads_mql_sal as (
    
    select   
        dim_conv.sk_conversion,
        dim_qa_lead.sk_qualified_lead,
        coalesce(dim_conv.email, dim_qa_lead.email) as email,
        coalesce(dim_conv.conversion_date, dim_qa_lead.converted_date) as conversion_date,
        coalesce(dim_conv.converted_month, dim_qa_lead.last_modified_month) as converted_month,
        dim_conv.count_as_station_lead as count_as_station_lead,
        dim_conv.count_as_crm_lead as count_as_crm_lead,
        dim_qa_lead.count_as_mql,
        dim_qa_lead.count_as_sal 
    from dim_conversions as dim_conv
    left join (
        select * from dim_qualified_leads
        where conversion_source_id is not null
        ) dim_qa_lead
        on dim_conv.conversion_source_id = dim_qa_lead.conversion_source_id
        and dim_conv.updated_at = dim_qa_lead.conversion_updated_at
    
    union all

    select
        null::numeric as sk_conversion,
        dim_qa_lead.sk_qualified_lead,
        dim_qa_lead.email as email,
        dim_qa_lead.converted_date as conversion_date,
        dim_qa_lead.last_modified_month as converted_month,
        0::numeric as count_as_station_lead,
        0::numeric as count_as_crm_lead,
        dim_qa_lead.count_as_mql,
        dim_qa_lead.count_as_sal
    from dim_qualified_leads as dim_qa_lead
    where dim_qa_lead.conversion_source_id is null
),

fat_final as (
    
    select
        row_number() over (order by conversion_date) as sk_fat_leads_mql_sal,
		TO_CHAR(conversion_date, 'yyyymmdd') as sk_tempo,
        sk_conversion,
        sk_qualified_lead,
        email,
        conversion_date,
        converted_month,
        count_as_station_lead,
        count_as_crm_lead,
        count_as_mql,
        count_as_sal
    from fat_leads_mql_sal
)

select * from fat_final