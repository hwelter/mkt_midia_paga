SELECT 
	goal_daily.sk_tempo,
	supermetrics.sk_supermetrics,
	goal_daily.sk_goal_daily,
	goal_daily.midia,
	goal_daily.account,
	goal_daily.country,
	supermetrics.media_cost,
	goal_daily.spend_daily as goal_daily_spend,
	goal_daily.conversions_daily as goal_daily_conversions,
	goal_daily.lead_daily as goal_daily_lead,
	goal_daily.mql_daily as goal_daily_mql,
	goal_daily.sal_daily as  goal_daily_sal,
	goal_daily.spend as goal_spend,
	goal_daily.conversions as goal_conversions,
	goal_daily.lead as goal_lead,
	goal_daily.mql as goal_mql,
	goal_daily.sal as goal_sal
FROM (
	SELECT max(sk_supermetrics) as sk_supermetrics,date, country, UPPER(type) as account, UPPER(media) as midia, sum(cost) as media_cost 
	FROM marketing_stg.dim_supermetrics 
	GROUP BY date, country, type, media
) AS supermetrics

INNER JOIN 

	(SELECT 	
			ROW_NUMBER () OVER (ORDER BY sk_tempo) as sk_goal_daily,
			sk_tempo,
			date,
			UPPER(media) as midia,
			country,
			strategy,
			UPPER(account) as account,
			(spend/(extract(day from last_day_of_month))) as spend_daily,
			(conversions/(extract(day from last_day_of_month))) as conversions_daily, 	
			(lead/(extract(day from last_day_of_month))) as lead_daily, 	
			(mql/(extract(day from last_day_of_month))) as mql_daily, 	
			(sal/(extract(day from last_day_of_month))) as sal_daily,
			spend,
			conversions,
			lead,
			mql,
			sal

	FROM marketing_stg.dim_goal 

	INNER JOIN (
				SELECT 	
					SUBSTRING(sk_tempo,0,7) AS ano_mes,	*
				FROM marketing_stg.dim_tempo
				) AS tempo

	ON 	(dim_goal.date = tempo.ano_mes)) AS goal_daily				  
ON ( 	goal_daily.sk_tempo like text(supermetrics.date) AND 
		goal_daily.midia = supermetrics.midia AND 
		goal_daily.account = supermetrics.account AND 
		goal_daily.country = supermetrics.country) 	
	